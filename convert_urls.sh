#!/bin/bash

# directory to operate on, should be empty for current folder and contain leading slash otherwise
dir="$1"

# wrap url links into primefaces theme format

# url('URL') -> url("#{resource['primefaces-ess-theme:dir/URL']}")
sed -i -r "s@url\('([^ #]+)'\)@url\(\"#{resource['primefaces-ess-theme:$dir\1']}\"\)@g" $dir*.css

# url('URL#ANCHOR') -> url("#{resource['primefaces-ess-theme:dir/URL']}#ANCHOR")
sed -i -r "s@url\('([^ #]+)#([^ #]+)'\)@url\(\"#{resource['primefaces-ess-theme:$dir\1']}#\2\"\)@g" $dir*.css
