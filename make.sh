#!/bin/bash

TOP=$PWD
THEME_SRC=src/main/ess_theme_template
THEME_TMP=build/META-INF/resources/primefaces-ess-theme
THEME_TARGET=src/main/resources/META-INF/resources/primefaces-ess-theme


# Copy starting definition

rm $THEME_TARGET -rf
cp $THEME_SRC $THEME_TARGET -r

# Generate PrimeFaces theme from Theme Roller jar using PrimeFaces theme converter

java -jar lib/ptc.jar config/ptc.prof

# Inject generated theme CSS and images

rm $THEME_TMP -rf
unzip build/ess-theme.jar -d build
sed -i -e "/JQUERY_UI_PLACEHOLDER/r ${THEME_TMP}/theme.css" $THEME_TARGET/theme.css
cp $THEME_TMP/images/* $THEME_TARGET/images
rm build -rf


# Patch URL references

# primefaces-ess-theme:"URL" -> primefaces-ess-theme:URL
sed -i -r 's@primefaces-ess-theme:\"([^ #"]+)\"@primefaces-ess-theme:\1@g' $THEME_TARGET/theme.css

convert_command="$TOP/convert_urls.sh"

cd $THEME_TARGET

. $convert_command

. $convert_command fonts/titillium/

. $convert_command fonts/opensans/opensans_bolditalic_macroman/
. $convert_command fonts/opensans/opensans_bold_macroman/
. $convert_command fonts/opensans/opensans_extrabolditalic_macroman/
. $convert_command fonts/opensans/opensans_extrabold_macroman/
. $convert_command fonts/opensans/opensans_italic_macroman/
. $convert_command fonts/opensans/opensans_lightitalic_macroman/
. $convert_command fonts/opensans/opensans_light_macroman/
. $convert_command fonts/opensans/opensans_regular_macroman/
. $convert_command fonts/opensans/opensans_semibolditalic_macroman/
. $convert_command fonts/opensans/opensans_semibold_macroman/

cd $TOP

# Unminify theme for debugging purposes
#cssunminifier $THEME_TARGET/theme.css $THEME_TARGET/theme.max.css